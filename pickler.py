# module de creation de fichier .pck 
# prend les fichiers du dossier 'data_to_pickle'
# construit les fichiers correspondants dans 'data'
import pickle

dataPath = "./data_to_pickle/"
picklePath = "./data/"

def getFile(fName):
    f = open(dataPath+fName, 'r')
    return f.read()

def getContentByLine(file):
    return file.split('\n')

def saveObject(name, content):
    # erase content of the file
    f = open(picklePath+name, 'w')
    f.close()
    # write final object inside
    f = open(picklePath+name, 'wb')
    pickle.dump(content, f)
    f.close()

# build pickle object from file
def createPickle(fName):
    file = getFile(fName)
    content = getContentByLine(file)
    name = fName.split('.')[0] + ".pck"
    saveObject(name, content)

def initPlayerFile():
    # erase content of the file
    saveObject("players.pck", [])

if __name__ == "__main__":
    # tab of all filenames to read
    # [filename, filename]
    # fileNames = ["beasts.txt", "types.txt", "weapon.txt", "armor.txt"]
    # foreach file
    # for file in fileNames:
    #    print(file)
    #    # create pickle object
    #    createPickle(file)
    initPlayerFile()

    print("finish")