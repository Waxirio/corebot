import random
import discord
from classes.Data import Data
from classes.MarketItem import MarketItem
from classes.Player import Player

class MarketController():

  # instance de la classe Data (singleton)
  instance = None

  ########### CONSTRUCTEUR ##########
  def __init__(self):
    # data path
    self._dataPath = "./data/"
    # load from file
    self._items = self.loadMeItem("market")
    self._dataPath = "./data/"
    self._lastId = 0
  
  ########### SINGLEON ##########
  # RETOURNE L'INSTANCE DE DATA #
  @staticmethod
  def getInstance():
    # Si l'inst,ce n'existe pas
    if(MarketController.instance == None):
      # La créer
      MarketController.instance = MarketController()
    # Retourner l'instance
    return MarketController.instance


  ##################################
  ########### GET  ITEMS ###########
  ##################################

  # select id marketItem
  def getItemByIdMarket(self, idMarket):
    res = []
    # loop in items
    for ele in self._items:
      if (ele._id == idMarket):
        res.append(ele)
    return res

  # select id player
  def getItemsByIdPlayer(self, idPlayer):
    res = []
    # loop in items
    for ele in self._items:
      if (ele._idPlayer == idPlayer):
        res.append(ele)
    return res

  # select item id
  def getItemsById(self, idItem):
    res = []
    # loop in items
    for ele in self._items:
      if (ele._item._id == idItem):
        res.append(ele)
    return res

  ##################################
  ########### LOAD ITEMS ###########
  ##################################

  # chargement du marche
  def loadMeItem(self, file):
    res = []
    data = ""
    # id:idPlayer:idItem:qty
    with open(self._dataPath+file, 'r') as file:
      data = file.readlines()
    # creation des elements du marche
    for ele in data:
      # ajout des elements
      res.append(self.loadMarketItem(ele))
    return res

  def loadMarketItem(self, data):
    splitedItem = data.split(':')
    # recuperation des elements
    idMarket = int(splitedItem[0])
    item = splitedItem[1].split(',')
    idPlayer = int(splitedItem[2])
    price = int(splitedItem[3])
    # fabrication de l'element du marche 
    item = Data.getInstance().getItemById(int(item[0])).new(int(item[1]))
    res = MarketItem(idMarket, item, idPlayer, price)
    self._lastId = idMarket+1
    return res

  # ajouter un nouvel item
  def addMarketItem(self, idPlayer, item, price):
    item = Data.getInstance().getItemById(item._id).new(item._amount)
    # nouvel id
    idMarket = self._lastId
    self._lastId += 1
    # creation d'un nouvel item market
    res = MarketItem(idMarket, item, idPlayer, price)
    # ajout dans la liste
    self._items.append(res)
    # sauvegarde
    self.saveMarketById(idMarket)

  # enleve l'item sur le marche d'id idMarket
  def removeMarketItemById(self, idMarket):
    count = 0
    # boucle dans le liste
    for i in self._items:
      if(i._id == idMarket):
        # supprime l'objet correspondant
        self._items.pop(count)
        self.deleteMarketItem(count)
      count += 1

  # delete line of market item nb
  def deleteMarketItem(self, nb):
    # open file
    with open(self._dataPath+"market", 'r') as file:
      data = file.readlines()
    # remove line
    data.pop(nb)
    # write file
    with open(self._dataPath+"market", 'w') as file:
      file.writelines( data )


  # sauvegarde un joueur d'id donné
  def saveMarketById(self, id):
    # recuperation de la save d'un item
    marketItem = self.getItemByIdMarket(id)[0].getSaveObject()
    # recuperation de la totalite du fichier de sauvegarde
    with open(self._dataPath+"market", 'r') as file:
      data = file.readlines()
    count = 0
    modified = False
    # boucle dans le fichier
    for line in data:
      # recuperation des infos
      if(int(line.split('#')[0]) == id): # si le premier terme est l'id de l'item
        data[count] = str(marketItem) # on modifie la ligne en cours
        modified = True # la modification à eu lieu
      count += 1
    if(not modified): # si pas de modification
      data.append(str(marketItem)) # alors c'est un nouvel item
    # recriture dans le fichier
    with open(self._dataPath+"market", 'w') as file:
      file.writelines( data )


  