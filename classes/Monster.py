import random
import discord
from classes.Armor import Armor
from classes.Weapon import Weapon

# Monster class

class Monster():

  # constructor
  def __init__(self, name, minLvl, maxLvl, drops):
    rd = random.randint(1, 100)
    self._maxLvl = maxLvl
    self._minLvl = minLvl
    self._level = random.randint(minLvl, maxLvl)
    self._currentLevel = self._level
    self._drops = drops
    self._defense = 0
    self._name = name

  def seFaireAttaquer(self, degats):
    # recupere les degats infliges
    # si la defense est moins grande il prend cher
    if(self._defense < degats):
      self._currentLevel -= (degats - self._defense)
    # sinon on fait plus rien
    return self._currentLevel

  def lootXp(self):
    # genere un montant d'xp
    return random.randint(self._level, self._level*10)

  def lootCoins(self):
    # genere un montant de pieces
    return random.randint(self._level, self._level*5)

  def lootDrop(self):
    finalDrop = []
    # boucle dans les drops
    for drop in self._drops:
      # recuperation de l'item (s'il est droppe)
      d = drop.getFinalDrop()
      # si il est droppe
      if(d != None):
        # sinon on l'ajoute
        finalDrop.append(d)
    return finalDrop

  def isDead(self):
    # Dit si le monstre est vaincu ou pas
    res = False
    if(self._currentLevel <= 0):
      res = True
    return res

  def getPower(self):
    # défini le niveau du monstre
    return (self._level + self._defense)

  # donnees affichables
  def toString(self):
    return str(self._name) + " " + str(self._minLvl) + "-" + str(self._maxLvl)

  def new(self):
    return self.__class__(self._name, self._minLvl, self._maxLvl, self._drops)

  # affichage du monstre detail (vie/viemax)
  def getEmbed(self):
    embed = discord.Embed(title=self._name, color=13046881)
    # creation de la chaine d'info
    infos = ""
    # Ajout du niveau
    infos += "**Niveau :** "+str(self._currentLevel)+"/"+str(self._level)+"\n"
    # Ajout de la défense
    infos += "**Défense :** "+str(self._defense)+"\n"
    # creation de la rubrique des infos
    embed.add_field(name="**Caractéristiques du monstre: **",
                    value=infos, inline=False)
    # retourne le final
    return embed
  
  # affichage du niveau seulement
  def getEmbedSpawn(self):
    embed = discord.Embed(title=self._name, color=13046881)
    # creation de la chaine d'info
    infos = ""
    # Ajout du niveau
    infos += "**Niveau :** "+str(self._currentLevel)+"\n"
    # Ajout de la défense
    infos += "**Défense :** "+str(self._defense)+"\n"
    # creation de la rubrique des infos
    embed.add_field(name="**Caractéristiques du monstre: **",
                    value=infos, inline=False)
    # retourne le final
    return embed
