import random
import pickle
from classes.Armor import Armor

# Armors class
class Legs(Armor):

  # constructor
  def __init__(self, id, level, name, stackable=False, amount=1):
    super().__init__(id, level, name, stackable, amount)
    self._level = level

  # creation d'un nouvel element
  def new(self, amount):
    return self.__class__(self._id, self._level, self._name, self._stackable, amount)

  # donnees affichables
  def toString(self):
    return str(self._name) + " ( **" + str(self._level) + "** )"

  def isWeapon(self):
    return False

  def isArmor(self):
    return True

  def isDrop(self):
    return False

  def isPotion(self):
    return False

  # Armures

  def isHelmet(self):
    return False

  def isBody(self):
    return False

  def isLegs(self):
    return True

  def isFoot(self):
    return False

  def getType(self):
    return "legs"