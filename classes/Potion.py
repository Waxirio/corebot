import random
import pickle
from classes.Item import Item

# Armors class
class Potion(Item):

  # constructor
  def __init__(self, id, name, defense, attaque, xp, stackable=True, amount=1):
    super().__init__(id, name, stackable, amount)
    self._defense = defense
    self._attaque = attaque
    self._xp = xp

  # nouvel element
  def new(self, amount):
    return self.__class__(self._id, self._name, self._defense, self._attaque, self._xp, self._stackable, amount)

  # donnees affichables
  def toString(self):
    carac = ""
    if(self._defense != 0):
      carac += "[def **" + str(self._defense) + "** ]"
    if(self._attaque != 0):
      carac += "[att **" + str(self._attaque) + "** ]"
    if(self._xp != 0):
      carac += "[xp **" + str(self._xp) + "** ]"
    return str(self._name) + carac

  def isWeapon(self):
    return False

  def isArmor(self):
    return False

  def isDrop(self):
    return False

  def isPotion(self):
    return True