import random

# Market Item class
class MarketItem():

  # constructor
  def __init__(self, id, item, idPlayer, price):
    self._id = id
    self._item = item
    self._idPlayer = idPlayer
    self._price = price

  # affichage de l'element
  def toString(self):
    return str(self._id) + ". " + str(self._item._name) + "(" + str(self._item._amount) + ") " + str(self._price) + " seekcoins"

  def getSaveObject(self):
    return  str(self._id) + ":" +\
            str(self._item._id) + "," + str(self._item._amount) +":" +\
            str(self._idPlayer) + ":" +\
            str(self._price)