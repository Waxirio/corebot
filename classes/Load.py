import discord

from classes.Player import Player
from classes.PlayerSave import PlayerSave
from classes.Monster import Monster
from classes.Craft import Craft

from classes.Item import Item
from classes.Weapon import Weapon
from classes.Potion import Potion
from classes.Drop import Drop

from classes.Armor import Armor
from classes.Helmet import Helmet
from classes.Legs import Legs
from classes.Body import Body
from classes.Foot import Foot

class Load():

  instance = None

  def __init__(self):
    self._dataPath = "./data/"

  @staticmethod
  def getInstance():
    if(Load.instance == None):
      Load.instance = Load()
    return Load.instance

  #######################
  ##   LOAD ELEMENTS   ##
  #######################

  ############## ITEMS ##############

  def loadMeItem(self, filename):
    res = []
    f = open(self._dataPath+filename, 'r')
    # loop on lines of file
    for line in f.readlines():
      l = line.replace('\n', '')
      # si ça commence par aure chose que # alors on lit la ligne
      if(not l.startswith('#')):
        res.append(self.newItem(l))
    f.close()
    return res

  def newItem(self, line):
    # item > type:id:nom
    splited = line.split(':')
    iType = str(splited[0])  # premier element (type)
    id = int(splited[1])  # second element (id)
    nom = str(splited[2])  # troisieme element (nom)
    res = None
    if(iType == "item"):
      res = Item(id, nom)
    elif(iType == "arme"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Weapon(id, power, nom)
    elif(iType == "armure"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Body(id, power, nom)
    elif(iType == "casque"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Helmet(id, power, nom)
    elif(iType == "jambiere"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Legs(id, power, nom)
    elif(iType == "botte"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Foot(id, power, nom)
    elif(iType == "potion"):
      defense = int(splited[3])  # quatrieme element (defense)
      attaque = int(splited[4])  # quatrieme element (attaque)
      xp = int(splited[5])  # quatrieme element (xp)
      res = Potion(id, nom, defense, attaque, xp)
    return res

  def getItemById(self, id):
    res = None
    for i in Data.getInstance()._items:
      if(i._id == id):
        res = i
    return res

  ############## CRAFTS ##############

  # id:arme:nom,puissance:materiaux,quantite+materiaux2,quantite:quantite produite
  def loadMeCraft(self, filename):
    res = []
    f = open(self._dataPath+filename, 'r')
    # loop on lines of file
    for line in f.readlines():
      # si ça commence par aure chose que # alors on lit la ligne
      if(not line.startswith('#')):
        c = self.newCraft(line)
        res.append(c)
    f.close()
    return res

  def newCraft(self, line):
    # item > id:nom
    splited = line.split(':')
    id = int(splited[0])  # premier element (id)
    itemId = int(splited[1])  # second element (itemId)
    ingredientList = str(splited[2])  # troisième element (all ingredients shiieeeeet)
    qtyProduced = str(splited[3])  # quatrième element (qty produced)
    #create objects
    item = self.getItemById(itemId).new(qtyProduced)
    ingredients = []
    ingSplit = ingredientList.split('+')
    # Loop in all ingredients
    for ing in ingSplit:
      # get elements
      ingDetail = ing.split(',')
      iId = int(ingDetail[0])
      iAmount = int(ingDetail[1])
      # create new item
      i = self.getItemById(iId).new(iAmount)
      # add this item to ingredient
      ingredients.append(i)
    return Craft(id, item, ingredients)

  def getCraftById(self, id):
    res = None
    for i in Data.getInstance()._crafts:
      if(i._id == id):
        res = i
    return res

  ############## MONSTRES ##############

  # nom,niveau min,niveau max:drop,quantite,proba+autre drop,quantite,proba
  def loadMeMonster(self, filename):
    res = [[],[],[],[],[]]
    f = open(self._dataPath+filename, 'r')
    # loop on lines of file
    for line in f.readlines():
      # si ça commence par aure chose que # alors on lit la ligne
      if(not line.startswith('#')):
        rare = self.getMonsterRarity(line)-1
        res[rare].append(self.newMonster(line))
    f.close()
    return res

  def getMonsterRarity(self, line):
    splitedLine = line.split(':')
    return int(splitedLine[2])

  def newMonster(self, line):
    # item > id:nom
    splited = line.split(':')
    monster = str(splited[0])  # premier element (monstre)
    drops = str(splited[1])  # second element (drops)
    # get name
    splitMonster = monster.split(',')
    nom = str(splitMonster[0]) # nom
    lvlMin = int(splitMonster[1]) # niveau minimum
    lvlMax = int(splitMonster[2]) # niveau maximum
    # get all drops
    allDrops = []
    dropList = drops.split('+')
    # loop for each loot
    for drop in dropList:
      dropDetail = drop.split(',')
      item = self.getItemById(int(dropDetail[0])).new(0) # get item
      maxAmount = int(dropDetail[1]) # get max amount
      DropRate = int(dropDetail[2]) # get drop rate
      # add new element to drop list
      allDrops.append(Drop(item, maxAmount, DropRate))
    # create new monster
    return Monster(nom, lvlMin, lvlMax, allDrops)

  ########## PLAYERS ##########

  def loadPlayers(self, filename):
    res = []
    f = open(self._dataPath+filename, 'r')
    for line in f.readlines():
      res.append(self.loadPlayerFromLine(line))
    f.close()
    return res

  def loadPlayerFromLine(self, line):
    splitedLine = line.split(':')
    # extract all properties
    id = int(splitedLine[0])
    nom = str(splitedLine[1])
    maxClass = int(splitedLine[2])
    maxRace = int(splitedLine[3])
    level = int(splitedLine[4])
    xp = int(splitedLine[5])
    coins = int(splitedLine[6])
    # class
    classe = None
    if(splitedLine[7] != ''):
      classe = self.getClassById(int(splitedLine[7]))
    # race
    race = None
    if(splitedLine[8] != ''):
      race = self.getRaceById(int(splitedLine[8]))
    # weapons
    weapons = []
    if(splitedLine[9] != ''):
      weapons = self.getItemsFromLine(splitedLine[9])
    # armors
    armors = []
    if(splitedLine[10] != ''):
      armors = self.getItemsFromLine(splitedLine[10])
    hands = int(splitedLine[11])
    foot = int(splitedLine[12])
    head = int(splitedLine[13])
    maxInventory = int(splitedLine[14])
    # inventory
    inventory = []
    if(splitedLine[15].strip() != ''):
      inventory = self.getItemsFromLine(splitedLine[15])

    p = Player(id, nom)
    p.fill(maxClass, maxRace, level, xp, coins, classe, race, weapons, armors, hands, foot, head, maxInventory, inventory)
    return p

  # retourne les armes equipees
  def getItemsFromLine(self, line):
    res = []
    splitedIds = line.split('+')
    for ele in splitedIds:
      splitedEle = ele.split(',')
      item = self.getItemById(int(splitedEle[0])).new(int(splitedEle[1]))
      res.append(item)
    return res

  # retourne la classe avec un id
  def getClassById(self, id):
    res = None
    for c in Data.getInstance()._class:
      if(c._id == id):
        res = c
    return res

  # retourne la race avec un id
  def getRaceById(self, id):
    res = None
    for r in Data.getInstance()._race:
      if(r._id == id):
        res = r
    return res