import random

# Drop class
class Drop():

  # constructor
  def __init__(self, item, maxAmount, dropRate):
    self._item = item
    self._maxAmount = maxAmount
    self._dropRate = dropRate

  # creation des drops finaux
  def getFinalDrop(self):
    nb = 0
    res = None
    # si le drop tombe bien dans le random
    if(random.randint(0,100) < self._dropRate):
      # nombre de drop
      nb = random.randint(0,self._maxAmount)
      # creation d'un nouvel item
      if(nb != 0):
        res = self._item.new(nb)
    return res