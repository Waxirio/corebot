import random
import pickle
from classes.Item import Item

# Armors class
class Armor(Item):

  # constructor
  def __init__(self, id, level, name, stackable=False, amount=1):
    super().__init__(id, name, stackable, amount)
    self._level = level

  # donnees affichables
  def toString(self):
    return str(self._name) + " [ **" + str(self._level) + "** ]"

  def isWeapon(self):
    return False

  def isArmor(self):
    return True

  def isDrop(self):
    return False

  def isPotion(self):
    return False
  
  def getType(self):
    return "armor"