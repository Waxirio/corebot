import random
import pickle

# Armors class
class Item():

  # constructor
  def __init__(self, id, name, stackable=True ,amount=1):
    self._id = id
    self._name = name
    self._stackable = stackable
    self._amount = amount

  # modification du nombre d'item
  def setAmount(self, amount):
    self._amount = amount

  # ajout d'un item à un item
  def add(self, item):
    res = False
    if(item._id == self._id):
      res = True
      self._amount += item._amount
    return res

  # creation d'un nouvel element
  def new(self, amount):
    return self.__class__(self._id, self._name, self._stackable, amount)

  # donnees affichables
  def toString(self):
    return str(self._name) + " [ **" + str(self._amount) + " **]"

  def isWeapon(self):
    return False

  def isArmor(self):
    return False

  def isDrop(self):
    return True

  def isPotion(self):
    return False