import discord
import pickle
import math
from classes.Race import Race
from classes.Classe import Classe
from classes.Armor import Armor
from classes.Weapon import Weapon
from classes.Monster import Monster
from classes.Item import Item
from classes.PlayerSave import PlayerSave

# Player class
class Player():

  # constructor
  def __init__(self, idPlayer, name):
    self._name = name
    self._idPlayer = idPlayer                       # id du joueur
    self._maxNbClasses = 1                          # races max du joueur
    self._maxNbRaces = 1                            # races max du joueur
    self._level = 1                                 # calcul du passage de niveau

    self._xp = 0                                    # niveau du joueur
    self._coins = 0                                 # argent du joueur  
    self._classe = None                               # classe du joueur
    self._race = None
    self._equWeapons = []                           # armes equipees
    self._equArmors = []                            # armures equipes

    self._nbHand = 2                                # nombre de mains
    self._nbFoot = 2                                # nombre de pieds
    self._nbHead = 1                                # nombre de tête

    self._maxInventory = 20                         # Quantite d'objets dans l'inventaire
    self._inventory = []                            # inventory
    self._thumbnail = ""

  # equipe l'armure
  def equipArmor(self, armor):
    self.spend([armor])
    t=armor.getType()
    # si il a une armure correspondante au type
    if(self.hasArmor(t)):
      # on la remplace
      self.remArmor(t)
    self._equArmors.append(armor)

  # dit si le player a deja ce type d'armure
  def hasArmor(self, aType):
    res = False
    # boucle dans les armures
    for a in self._equArmors:
      # si le type correspond alors
      if (a.getType() == aType):
        # il l'a deja
        res = True
    return res

  # supprime l'armure equipe du type donne
  def remArmor(self, aType):
    count = 0
    # boucle dans les armures
    for a in self._equArmors:
      # si le type est valide
      if (a.getType() == aType):
        # ajout de l'item dans l'inventaire
        self.addItemsToInventory([self._inventory[count]])
        # supprimer l'item dans l'equipement
        self._equArmors.pop(count)
      count += 1
      
  def equipWeapon(self, weapon):
    # enleve l'arme actuelle
    self.spend([weapon])
    # si une arme est deja equipee
    if(len(self._equWeapons) >= 1):
      # on met l'arme deja equipee dans l'inventaire
      self.addItemsToInventory(self._equWeapons)
      self._equWeapons = []
    # equipe l'arme
    self._equWeapons.append(weapon)    

  # depense les ingredients donnes
  def spend(self, ingredients):
    # boucle dans les ingredients
    for i in ingredients:
      # pour chacun enleve l'item
      self.remItem(i)
    # si des items on un nombre de 0
    self.cleanInventory()

  # ajoute une liste d'item dans l'inventaire
  def addItemsToInventory(self, items):
    res = True
    # boucle dans les items
    for i in items:
      # si il a les items, on les additionnent
      if(self.checkHasItem(i._id, 1) and i._stackable):
        self.addItem(i)
      # si l'inventaire est plein on peux pas les ajouter
      elif(len(self._inventory) >= self._maxInventory):
        res = False
      else:
        # sinon on l'ajoute séparément
        self._inventory.append(i)
    return res

  # peut depenser de l'argent
  def canAfford(self, amount):
    return self._coins >= amount

  def spendMoney(self, amount):
    self._coins -= amount

  def addMoney(self, amount):
    self._coins += amount

  def getItemById(self, id):
    res = None
    if(id < len(self._inventory) and id >= 0):
      res = self._inventory[id]
    return res

  # ajoute l'item
  def addItem(self, item):
    for ele in self._inventory:
      # correspondance de l'id
      if(ele._id == item._id):
        # on ajoute l'item
        ele._amount += item._amount
    
  # supprime l'item de l'inventaire
  def remItem(self, item):
    # boucle
    for ele in self._inventory:
      # si l'id est celui de l'item
      if(ele._id == item._id):
        # on le supprime/enleve un montant
        ele._amount -= item._amount

  # ajout de l'image
  def setThumbnail(self, img):
    self._thumbnail = img

  # check si le player a l'item id et de quantite amount
  def checkHasItem(self, id, amount):
    res = False
    # boucle dans l'inventaire
    for item in self._inventory:
      # si l'id est présent et que sa quantite est présente
      if(item._id == id and item._amount >= amount):
        res = True
    return res

  def cleanInventory(self):
    count = 0
    # supprime de l'inventaire les elements
    for i in self._inventory:
      # dont la quantite est de 0
      if(i._amount <= 0):
        self._inventory.pop(count)
      count += 1

  # reset de l'inventaire
  def resetInventory(self):
    self._inventory = []

  # attaque un monstre en lui donnant les degats
  def attaquer(self, monster):
    if(monster != None):
      monster.seFaireAttaquer(self.getDegats())
    return monster

  # recuperation d'un niveau
  def getLevel(self):
    xpAmount = 40
    level = 1
    # tant qu'on peut monter
    while xpAmount <= self._xp:
      # on ajoute l'xp et le niveau
      level += 1
      xpAmount = round(xpAmount*2.2)
    return [level,xpAmount]

  # ajout des coins
  def addCoins(self, amount):
    self._coins = int(self._coins + amount)

  # ajout de l'xp
  def addXp(self, amount):
    res = False
    # ajout
    self._xp = int(self._xp + amount)
    # si le calcul du level donne un niveau supp
    if(self.getLevel()[0] > self._level):
      # nouveau niveau !
      res = True
      self._level = self.getLevel()[0]
    return res

  def getDegats(self):
    # init degat
    degats = self._level
    ##### calcul des degats des armes #####
    for w in self._equWeapons:
      # pour toutes les armes du joueur on additionne leur degats
      wDegs = w._level
      # on ajoute aux degats totaux
      degats += wDegs
    ##### On ajoute la puissance des armures #####
    for a in self._equArmors:
      # pour toutes les armures du joueur on additionne leur puissance
      aDegs = a._level
      # on ajoute aux degats totaux
      degats += aDegs
    return degats

  def getEmbed(self):
    embed = discord.Embed(title=self._name, color=16777215)
    # creation de la chaine d'info
    infos = ""
    # Ajout de l'xp
    infos += "**Xp:** "+str(self._xp)+"/"+str(self.getLevel()[1])+"\n"
    # Ajout du niveau
    infos += "**Level:** "+str(self.getLevel()[0])+"\n"
    # Ajout de la monnaie
    infos += "**Seekoins:** "+str(self._coins)+"\n"

    # Ajout des classes
    infos += "**Classes:**\n"
    if(self._classe != None):
      infos += self._classe._name+"\n"

    # Ajout des races
    infos += "**Races:**\n"
    if(self._race != None):
      infos += self._race._name+"\n"

    # Ajout des armes
    infos += "**Armes:**\n"
    for w in self._equWeapons:
        infos += w._name + " (" + str(w._level) + ")\n"

    # Ajout des armures
    infos += "**Armures:**\n"
    for a in self._equArmors:
        infos += a._name + " (" + str(a._level) + ")\n"
    
    # Ajout de l'inventaire
    infos += "**Inventaire:** "+ str(len(self._inventory)) + "/" + str(self._maxInventory)+"\n"

    # Ajout de l'inventaire
    count = 0
    infos += "**Détail:**\n"
    for i in self._inventory:
        infos += "> " + str(count) + ". " + i.toString()+"\n"
        count += 1
    # creation de la rubrique des infos
    embed.add_field(name="**Fiche**", value=infos, inline=False)
    # retourne le final
    return embed

  # fonction de remplissage de la classe
  def fill(self, maxClass, maxRace, level, xp, coins, classe, race, weapons, armors, hands, foot, head, maxInventory, inventory, thumb):
    self._maxNbClasses = maxClass
    self._maxNbRaces = maxRace
    self._level = level
    self._xp = xp
    self._coins = coins
    self._classe = classe
    self._race = race
    self._equWeapons = weapons
    self._equArmors = armors
    self._nbHand = hands
    self._nbFoot = foot
    self._nbHead = head
    self._maxInventory = maxInventory
    self._inventory = inventory
    self._thumbnail = thumb

  # retourne l'objet sauvegardée
  def getSave(self):
    return PlayerSave(self)