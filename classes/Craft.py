import discord
from discord.ext import commands
import random
import os, sys
from classes.Item import Item
from classes.Player import Player

class Craft():

  # constructor
  def __init__(self, id, item, ingredient):
    self._id = id
    self._finalItem = item
    self._ingredient = ingredient

  # check si le craft est valable
  def canBeCrafted(self, player):
    res = True
    # boucle dans les ingredients
    for i in self._ingredient:
      # si le joueur a pas l'element
      if(not player.checkHasItem(i._id, i._amount)):
        # il n'est pas realisable
        res = False
    return res

  # donnees affichables
  def toString(self):
    ingredients=""
    # boucle dans les ingredients
    for i in self._ingredient:
      # données de l'ingredient
      ingredients += "> " + str(i._name) + " [ **" + str(i._amount) + "** ]\n"
    return "**"+self._finalItem._name+"**\n"+ingredients

  def toStringWithPlayer(self, player):
    # presentation
    itemPresentation = "**"+self._finalItem._name+"** "
    if(self._finalItem.isArmor() or self._finalItem.isWeapon()):
      itemPresentation += "(" + str(self._finalItem._level) + ")"
    itemPresentation += '\n'
    # ingredients
    ingredients=""
    for i in self._ingredient:
      # si le joueur a l'item alors
      # ajouter le white mark
      if(player.checkHasItem(i._id, i._amount)):
        mark = ":white_check_mark:"
      else:
        mark = ":x:"
      # ajout de l'ingredient
      ingredients += "> " + mark + " " + str(i._name) + " [ **" + str(i._amount) + "** ]\n"
    return itemPresentation+ingredients

  def getName(self):
    return self._finalItem._name

