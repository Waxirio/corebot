import pickle
import discord
import random
from classes.Player import Player
from classes.PlayerSave import PlayerSave
from classes.Monster import Monster
from classes.Craft import Craft

from classes.Item import Item
from classes.Weapon import Weapon
from classes.Potion import Potion
from classes.Drop import Drop

from classes.Armor import Armor
from classes.Helmet import Helmet
from classes.Legs import Legs
from classes.Body import Body
from classes.Foot import Foot

# from classes.Load import Load

# Data class

class Data():

  # instance de la classe Data (singleton)
  instance = None

  ########### CONSTRUCTEUR ##########
  def __init__(self):
    # monsters elements
    self._currentMonster = {}
    self._involvedPlayers = {}
    self._battleOpen = {}
    # data path
    self._dataPath = "./data/"
    # load from file
    self._items = self.loadMeItem("items")
    self._crafts = self.loadMeCraft("crafts")
    self._beasts = self.loadMeMonster("monsters")
    self._class = []
    self._race = []
    # load players
    self._players = self.loadPlayers("players")
  
  ########### SINGLEON ##########
  # RETOURNE L'INSTANCE DE DATA #
  @staticmethod
  def getInstance():
    # Si l'inst,ce n'existe pas
    if(Data.instance == None):
      # La créer
      Data.instance = Data()
    # Retourner l'instance
    return Data.instance

  ###############################
  ######      PLAYERS      ######
  ###############################

  # creation d'un nouveau joueur
  def newPlayer(self, id, name):
    res = False
    # si l'utilisateur n'a pas de personnage
    if(self.getPlayerById(id) == None):
      # creation de joueur
      pNew = Player(id, name)
      # ajout du personnage a la liste
      self._players.append(pNew)
      # variable de resultat
      res = True
      # sauvegarde du bon perso
      self.savePlayerById(id)
    return res

  # rechercher un joueur par id
  def getPlayerById(self, id):
    res = None
    # boucle dans la liste des joueurs
    for p in self._players:
      # retourne le joueur du bon id
      if(p._idPlayer == id):
        # variable de resultat
        res = p
    return res

    # supprime un joueur par son id
  def deletePlayerById(self, id):
    count = 0
    res = False
    # boucle dans tous les joueurs
    for p in self._players:
      # si l'id des joueurs correspondent
      if(p._idPlayer == id):
        # on le supprime
        self._players.pop(count)
        res = True
      # variable de boucle
      count += 1
    return res

  ###############################
  ######      BATTLE       ######
  ###############################

  # Get monster
  def getMonster(self, id):
    # Retourne le monstre se trouvant dans le channel id
    return self._currentMonster[id]

  # Nouveau monstre
  def spawnMonster(self, channel):
    channelId = channel.id
    # Creation du tableau de combattants
    self._involvedPlayers[channelId] = []
    # Creation du monstre dans la bonne room
    self._currentMonster[channelId] = self.selectMonster()
    # Ouverture du combat
    self._battleOpen[channelId] = True
    # Affichage de ce qu'il faut
    print("Nouveau monstre: " + str(channel.name) + " " + str(self._currentMonster[channelId].toString()))

  # spawn d'un monstre
  def selectMonster(self):
    # calcul des probabilités de spawn d'un monstre
    monsterRd = random.randint(0,1000)
    # calcul du niveau de rareté (1-commun, 5-legendaire)
    level = 0
    if(monsterRd < 700): # level 1 autre
      level = 0
    elif(monsterRd < 840): # level 2 14%
      level = 1
    elif(monsterRd < 940): # level 3 10%
      level = 2
    elif(monsterRd < 990): # level 4 5% 
      level = 3
    elif(monsterRd < 1000): # level 5 1%
      level = 4
    # spawn d'un monstre du bon niveau de rarete
    monsterList = self._beasts[level]
    # random dans les monstres de la meme rarete
    rd = random.randint(0, len(monsterList)-1)
    # retourne le monstre final
    res = monsterList[rd].new()
    # le renvoie
    return res

  # ajoute un nouveau combattant dans le combat
  async def newInvolvedPlayer(self, newPlayer, channelId):
    res = False # variable de resultat
    add = True # variable de resultat
    # si le channel est encore en combat
    if(channelId in self._currentMonster and self._battleOpen[channelId]):
      # calcul des forces deja presentes dans le combat
      for player in self._involvedPlayers[channelId]:
        # si il est deja dans le combat
        if newPlayer._idPlayer == player._idPlayer:
          # abandonner
          add = False
      # si on peux l'ajouter et qu'il n'est pas null
      if add and newPlayer != None:
        # ajouter le nouveau joueur
        self._involvedPlayers[channelId].append(newPlayer)
        # et il attaque le monstre en combat
        self._currentMonster[channelId] = newPlayer.attaquer(self.getMonster(channelId))
        res = True
    return res

  # calcul la fin du combat
  async def checkBattleEnd(self, channelId):
    res = False
    # si le monstre est present et pas mort
    if(channelId in self._currentMonster):
      if(self._currentMonster[channelId].isDead()):
        # reset les parametres de combat
        res = True
    return res

  # fin du combat
  def initForNextBattle(self, channelId):
    # sauvegarde des joueurs après la fin du combat
    for player in self._involvedPlayers[channelId]:
      self.savePlayerById(player._idPlayer)
    # reinitialisation des parametres de combat
    self._involvedPlayers.pop(channelId, None)
    self._currentMonster.pop(channelId, None)

  # calcul d'investissement des joueurs dans le combat
  def getPlayerPercentageInBattle(self, player, monster):
    deg = 0
    if(monster != None):
      # recuperation des degats
      deg = player.getDegats()
      # recuperation du pouvoir
      pow = monster.getPower()
      if(deg > pow):
        deg = pow
      # calcul de l'investissement du personnage
    return ((deg*100)/pow)/100

  ###############################
  ######       LOOTS       ######
  ###############################

  # recupere le monstre et le supprime
  def getMonsterAndClean(self, channelId):
    m = self._currentMonster[channelId] # monstre
    self._currentMonster.pop(channelId, None) # suppression du monstre
    return m

  def getCurrentMonsterLootCoins(self, m):
    return m.lootCoins() # recupere les loots en monnaie d'un monstre

  def getCurrentMonsterLootXp(self, m):
    return m.lootXp() # recupere les loots en xp d'un monstre

  def getCurrentMonsterLootDrop(self, m):
    return m.lootDrop() # recupere les loots en drops d'un monstre

  ###############################
  ######   SAVE ELEMENTS   ######
  ###############################

  # sauvegarde tous les joueurs
  def savePlayers(self):
    # clear save file
    f = open(self._dataPath+"players", 'w')
    f.close()
    # open file
    f = open(self._dataPath+"players", 'w')
    # boucle dans les joueurs
    for player in self._players:
      # recupere la bonne forme du joueur
      save = PlayerSave(player).getSaveObject()
      # l'ecrit dans le fichier
      f.write(save)
    # sauvegarde du fichier
    f.close()

  # sauvegarde un joueur d'id donné
  def savePlayerById(self, id):
    # recuperation de la save d'un joueur
    player = PlayerSave(self.getPlayerById(id)).getSaveObject()
    # recuperation de la totalite du fichier de sauvegarde
    with open(self._dataPath+"players", 'r') as file:
      data = file.readlines()
    count = 0
    modified = False
    # boucle dans le fichier
    for line in data:
      # recuperation des infos
      if(int(line.split('#')[0]) == id): # si le premier terme est l'id du joueur
        data[count] = str(player) # on modifie la ligne en cours
        modified = True # la modification à eu lieu
      count += 1
    if(not modified): # si pas de modification
      data.append(str(player)) # alors c'est un nouveau joueur
    # recriture dans le fichier
    with open(self._dataPath+"players", 'w') as file:
      file.writelines( data )

  ###############################
  ######   LOAD ELEMENTS   ######
  ###############################

  ############## ITEMS ##############

  def loadMeItem(self, filename):
    res = []
    f = open(self._dataPath+filename, 'r')
    # boucle dans le fichier
    for line in f.readlines():
      l = line.replace('\n', '')
      # si ça commence par aure chose que # alors on lit la ligne
      if(not l.startswith('#')):
        # on cree un nouvel item
        res.append(self.newItem(l))
    f.close() # on ferme le fichier
    return res

  def newItem(self, line):
    # item > type:id:nom
    splited = line.split(':')
    iType = str(splited[0])  # premier element (type)
    id = int(splited[1])  # second element (id)
    nom = str(splited[2])  # troisieme element (nom)
    res = None
    # si c'est un item
    if(iType == "item"): 
      res = Item(id, nom)
    # si c'est une arme
    elif(iType == "arme"): 
      power = int(splited[3])  # quatrieme element (puissance)
      res = Weapon(id, power, nom)
    # si c'est une armure
    elif(iType == "armure"): 
      power = int(splited[3])  # quatrieme element (puissance)
      res = Body(id, power, nom)
    # si c'est un casque
    elif(iType == "casque"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Helmet(id, power, nom)
    # si c'est une jambiere
    elif(iType == "jambiere"):
      power = int(splited[3])  # quatrieme element (puissance)
      res = Legs(id, power, nom)
    # si c'est une botte
    elif(iType == "botte"): 
      power = int(splited[3])  # quatrieme element (puissance)
      res = Foot(id, power, nom)
    # si c'est une potion
    elif(iType == "potion"): 
      defense = int(splited[3])  # quatrieme element (defense)
      attaque = int(splited[4])  # quatrieme element (attaque)
      xp = int(splited[5])  # quatrieme element (xp)
      res = Potion(id, nom, defense, attaque, xp)
    return res

  def getItemById(self, id):
    res = None
    # boucle dans tous les items
    for i in self._items:
      # si c'est l'id correspondant
      if(i._id == id):
        # mettre le resultat a jour
        res = i
    return res

  ############## CRAFTS ##############

  # id:arme:nom,puissance:materiaux,quantite+materiaux2,quantite:quantite produite
  def loadMeCraft(self, filename):
    res = []
    f = open(self._dataPath+filename, 'r')
    # loop on lines of file
    for line in f.readlines():
      # si ça commence par autre chose que # alors on lit la ligne
      if(not line.startswith('#')):
        # on créer un nouveau craft
        c = self.newCraft(line)
        res.append(c)
    f.close()
    return res

  # creation d'un nouveau craft
  def newCraft(self, line):
    # item > id:nom
    splited = line.split(':')
    id = int(splited[0])  # premier element (id)
    itemId = int(splited[1])  # second element (itemId)
    ingredientList = str(splited[2])  # troisième element (all ingredients shiieeeeet)
    qtyProduced = int(splited[3])  # quatrième element (qty produced)
    # creation de l'objet
    item = self.getItemById(itemId).new(qtyProduced) # nouvel objet d'une quantité specifiee
    ingredients = self.getItemsFromLine(ingredientList)
    return Craft(id, item, ingredients)

  # redonne le craft d'id correspondant
  def getCraftById(self, id):
    res = None
    # boucle dans les crafts
    for i in self._crafts:
      # si c'est le bon id donner le bon craft
      if(i._id == id):
        res = i
    return res

  ############## MONSTRES ##############

  # nom,niveau min,niveau max:drop,quantite,proba+autre drop,quantite,proba
  def loadMeMonster(self, filename):
    # creation du tableau des monstres
    res = [[],[],[],[],[]]
    f = open(self._dataPath+filename, 'r')
    # boucle dans le fichier
    for line in f.readlines():
      # si ça commence par aure chose que # alors on lit la ligne
      if(not line.startswith('#')):
        # on check la rareté du monstre
        rare = self.getMonsterRarity(line)-1
        # on ajoute le monstre dans la bonne catégorie
        res[rare].append(self.newMonster(line))
    f.close()
    return res

  # recuperation de la rarete des monstres
  def getMonsterRarity(self, line):
    splitedLine = line.split(':')
    return int(splitedLine[2])

  def newMonster(self, line):
    # item > id:nom
    splited = line.split(':')
    monster = str(splited[0])  # premier element (monstre)
    drops = str(splited[1])  # second element (drops)
    # recuperation du nom
    splitMonster = monster.split(',')
    nom = str(splitMonster[0]) # nom
    lvlMin = int(splitMonster[1]) # niveau minimum
    lvlMax = int(splitMonster[2]) # niveau maximum
    # recuperation des drops des monstres
    allDrops = []
    dropList = drops.split('+')
    # boucle dans tous les drops
    for drop in dropList:
      dropDetail = drop.split(',')
      item = self.getItemById(int(dropDetail[0])).new(0) # item
      maxAmount = int(dropDetail[1]) # montant max
      DropRate = int(dropDetail[2]) # drop rate
      # ajout du nouveau drop
      allDrops.append(Drop(item, maxAmount, DropRate))
    # creation d'un nouveau monstre
    return Monster(nom, lvlMin, lvlMax, allDrops)

  ########## PLAYERS ##########

  # chargement de tous les joueurs
  def loadPlayers(self, filename):
    res = []
    f = open(self._dataPath+filename, 'r')
    # boucle dans toutes les lignes
    for line in f.readlines():
      # chargement du player avec la ligne
      res.append(self.loadPlayerFromLine(line))
    f.close()
    return res

  def loadPlayerFromLine(self, line):
    splitedLine = line.split("#")
    # extraction des informations
    id = int(splitedLine[0]) # id
    nom = str(splitedLine[1]) # nom
    maxClass = int(splitedLine[2]) # nombre max de classe
    maxRace = int(splitedLine[3]) # nombre max de race
    level = int(splitedLine[4]) # niveau
    xp = int(splitedLine[5]) # xp
    coins = int(splitedLine[6]) # pieces
    # class
    classe = None
    if(splitedLine[7] != ''):
      classe = self.getClassById(int(splitedLine[7]))
    # race
    race = None
    if(splitedLine[8] != ''):
      race = self.getRaceById(int(splitedLine[8]))
    # weapons
    weapons = []
    if(splitedLine[9] != ''):
      weapons = self.getItemsFromLine(splitedLine[9])
    # armors
    armors = []
    if(splitedLine[10] != ''):
      armors = self.getItemsFromLine(splitedLine[10])
    hands = int(splitedLine[11]) # nombre de mains
    foot = int(splitedLine[12]) # nombre de pieds
    head = int(splitedLine[13]) # nombre de tete
    maxInventory = int(splitedLine[14]) # nombre max dans l'inventaire
    # inventory
    inventory = []
    if(splitedLine[15].strip() != ''):
      # recuperation de l'inventaire a partir de la ligne
      inventory = self.getItemsFromLine(splitedLine[15])
    thumbnail = str(splitedLine[16])
    p = Player(id, nom)
    # affectation de toutes les infos au player
    p.fill(maxClass, maxRace, level, xp, coins, classe, race, weapons, armors, hands, foot, head, maxInventory, inventory, thumbnail)
    return p

  # retourne les armes equipees
  def getItemsFromLine(self, line):
    res = []
    # recuperation des items separement
    splitedIds = line.split('+')
    # boucle pour chaque item
    for ele in splitedIds:
      splitedEle = ele.split(',')
      # creation d'un nouvel item de quantite splitedEle[1]
      item = self.getItemById(int(splitedEle[0])).new(int(splitedEle[1]))
      res.append(item)
    return res

  # retourne la classe avec un id
  def getClassById(self, id):
    res = None
    # boucle dans les classes
    for c in self._class:
      # recuperation de la bonne classe
      if(c._id == id):
        res = c
    return res

  # retourne la race avec un id
  def getRaceById(self, id):
    res = None
    # boucle dans les races
    for r in self._race:
      # recuperation de la bonne race
      if(r._id == id):
        res = r
    return res