import discord
import pickle
import math
from classes.Race import Race
from classes.Classe import Classe

class PlayerSave():

  # constructor
  def __init__(self, player):
    self._player = player
    self._save = "./data/player"
    self._separator = '#'

  # recuperation de l'item
  def getItemId(self, items):
    res = ""
    for i in items:
      res += str(i._id) + "," + str(i._amount)
      res += "+"
    return res[:-1]

  # recuperation de la classe
  def getClasseId(self, player):
    res = ""
    if(player._race != None):
      res = player._race._id
    return res

  # recuperation de la race
  def getRaceId(self, player):
    res = ""
    if(player._classe != None):
      res = player._classe._id
    return res

  # creation de la ligne de sauvegarde
  def getSaveObject(self):
    return  str(self._player._idPlayer)+self._separator+\
            str(self._player._name.strip())+self._separator+\
            str(self._player._maxNbClasses)+self._separator+\
            str(self._player._maxNbRaces)+self._separator+\
            str(self._player._level)+self._separator+\
            str(self._player._xp)+self._separator+\
            str(self._player._coins)+self._separator+\
            str(self.getClasseId(self._player))+self._separator+\
            str(self.getRaceId(self._player))+self._separator+\
            str(self.getItemId(self._player._equWeapons))+self._separator+\
            str(self.getItemId(self._player._equArmors))+self._separator+\
            str(self._player._nbHand)+self._separator+\
            str(self._player._nbFoot)+self._separator+\
            str(self._player._nbHead)+self._separator+\
            str(self._player._maxInventory)+self._separator+\
            str(self.getItemId(self._player._inventory))+self._separator+\
            str(self._player._thumbnail)
