import random
import pickle
from classes.Item import Item

# Armors class
class Weapon(Item):

  # constructor
  def __init__(self, id, level, name, stackable=False, amount=1):
    super().__init__(id, name, stackable, amount)
    self._level = level

  # nouvel element
  def new(self, amount):
    return self.__class__(self._id, self._level, self._name, self._stackable, amount)

  # donnees affichables
  def toString(self):
    return str(self._name) + "( ** " + str(self._level) + "** )"

  def isWeapon(self):
    return True

  def isArmor(self):
    return False

  def isDrop(self):
    return False
  
  def isPotion(self):
    return False