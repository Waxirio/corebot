import discord
import json
import random
from discord.ext import commands
from dotenv import load_dotenv
from classes.Data import Data
import sys
import os.path

# bot client init
load_dotenv()

# get token and prefix from env
TOKEN = os.getenv('DISCORD_TOKEN')
PREFIX = os.getenv('DISCORD_PREFIX')

# create bot 
client = commands.Bot(command_prefix = PREFIX)

dataInstance = Data.getInstance()

# load cogs
@client.command()
async def load(ctx, extension):
    if(ctx.message.author.id == 294917236634943490):
        client.load_extension(f'cogs.{extension}')

# reload cogs
@client.command()
async def reload(ctx, extension):
    if(ctx.message.author.id == 294917236634943490):
        client.unload_extension(f'cogs.{extension}')
        client.load_extension(f'cogs.{extension}')
        ctx.send("Les commandes ont étés rechargées !")

# unload cogs
@client.command()
async def unload(ctx, extension):
    if(ctx.message.author.id == 294917236634943490):
        client.unload_extension(f'cogs.{extension}')

# remove basic help command
client.remove_command('help')
# loop in all files in cogs
for filename in os.listdir('./cogs'):
    if filename.endswith('.py'):
        client.load_extension(f'cogs.{filename[:-3]}')

# run server
client.run(TOKEN)
    