import discord
from discord.ext import commands
import random
import os, sys

sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), 
        os.path.pardir)))
from classes.Data import Data

class Beast(commands.Cog):

  def __init__(self, client):
    self.client = client

  # Events
  @commands.Cog.listener()
  async def on_ready(self):
    print("Cog loaded: beast")
  
  # Commands
  @commands.command()
  async def beast(self, ctx):
    # recuperation du channel
    channelId = ctx.channel.id
    # envoi des données du monstre correspondant
    if(channelId in Data.getInstance()._currentMonster):
      await ctx.send(embed=Data.getInstance()._currentMonster[channelId].getEmbed())
    else:
      # sinon pas de monstre
      embed = discord.Embed(title="Recherche du monstre: ", color=15092992)
      embed.add_field(name="Resultat", value="Aucun monstre n'est encore debout", inline=False)
      await ctx.send(embed=embed)

  
def setup(client):
  client.add_cog(Beast(client))