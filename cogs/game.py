from classes.Data import Data
import discord
from discord.ext import commands
import random
import os
import sys

sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__),
                     os.path.pardir)))


class Game(commands.Cog):

  def __init__(self, client):
    self.client = client

  # Events
  @commands.Cog.listener()
  async def on_ready(self):
    print("Cog loaded: game")

  # commande sur tous les messages
  @commands.Cog.listener()
  async def on_message(self, message):
    # si c'est pas lui meme
    if(message.author.id != 662650993640603678):
      # si c'est un message vraiment normal
      if(not message.content.startswith(self.client.command_prefix) and message.guild != None):
        # on calcul l'apparition d'un monstre
        if(random.randint(0, 100) <= 50):
          # ajout du monstre
          Data.getInstance().spawnMonster(message.channel)
          await message.channel.send(embed=Data.getInstance().getMonster(message.channel.id).getEmbedSpawn())

  # Commands
  @commands.command()
  async def cookie(self, ctx):
    await ctx.send(":cookie:")

  # Commands
  @commands.command()
  async def wequ(self, ctx):
    # recuperation des infos
    player = Data.getInstance().getPlayerById(ctx.author.id)
    nb = self.checkPlayerInventory(ctx, player)
    embed = discord.Embed(title="Arme equipee", color=12848654)
    # si on a un nombre
    if(nb != None):
      # si l'item est une arme
      if(player._inventory[nb].isWeapon()):
        # on l'equipe
        text = "Nouvelle arme équipée: " + str(player._inventory[nb]._name)
        embed.add_field(name="Arme", value=text, inline=False)
        player.equipWeapon(player._inventory[nb])
      else:
        # sinon erreur
        embed.add_field(name="Arme", value="Impossible d'equiper l'arme", inline=False)
    else:
      embed.add_field(name="Arme", value="Impossible d'equiper l'arme", inline=False)
    # envoi du message
    await ctx.message.channel.send(embed=embed)
    Data.getInstance().savePlayerById(player._idPlayer)

  # Commands
  @commands.command()
  async def aequ(self, ctx):
    # recuperation des infos
    player = Data.getInstance().getPlayerById(ctx.author.id)
    nb = self.checkPlayerInventory(ctx, player)
    embed = discord.Embed(title="Armure equipee", color=12848654)
    # si on a un nombre
    if(nb != None):
      # check si c'est une armure
      if(player._inventory[nb].isArmor()):
        text = "Nouvelle Armure équipée: " + str(player._inventory[nb]._name)
        embed.add_field(name="Armure", value=text, inline=False)
        player.equipArmor(player._inventory[nb])
      else:
        # sinon erreur
        embed.add_field(name="Armure", value="Impossible d'equiper l'armure", inline=False)
    else:
        # sinon erreur
        embed.add_field(name="Armure", value="Impossible d'equiper l'armure", inline=False)
    # envoi du message
    await ctx.message.channel.send(embed=embed)
    Data.getInstance().savePlayerById(player._idPlayer)

  # check si le nombre est valide (compris dans les bornes de l'inventaire)
  def checkPlayerInventory(self, ctx, player):
    arg = self.getArgs(ctx.message.content)
    nb = None
    # si le str est de type nombre
    if(len(arg) != 0 and arg[0].isnumeric()):
      # on le prend
      nb = int(arg[0])
      # si il est en dehors des bornes, on l'annule
      if(nb >= len(player._inventory) or nb <= 0):
        nb = None
    return nb

  def getArgs(self, texte):
    args = texte.split(' ')
    args.pop(0)
    return args


def setup(client):
  client.add_cog(Game(client))
