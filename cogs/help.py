import discord
from discord.ext import commands
import random
import os, sys

sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__), 
        os.path.pardir)))
from classes.Data import Data

class Help(commands.Cog):

  def __init__(self, client):
    self.client = client
    self._helpContent = "./data/help/helpinfo"
    self._helpFolder = "./data/help/"

  # Events
  @commands.Cog.listener()
  async def on_ready(self):
    print("Cog loaded: help")
  
  # Commands
  @commands.command()
  async def help(self, ctx):
    args = self.getArgs(ctx.message.content)
    # recuperation du message initial
    helpMessage = open(self._helpContent, 'r').read()
    # si des arguments
    if(len(args) != 0):
      # recherche du bon fichier
      pathToFile = self._helpFolder + str(args[0])
      # si le fichier existe
      if os.path.isfile(pathToFile):
        # rempli le message du bon contenu
        helpMessage = open(pathToFile, 'r').read()
      else:
        # pas de fichier
        helpMessage = "```No command named "+ str(args[0]) +"```"
    await ctx.channel.send("A private message was send !")
    # get correct user
    user = ctx.author
    # if ok send embed message
    if user is not None:
      await user.send(helpMessage)

  def getArgs(self, texte):
    args = texte.split(' ')
    args.pop(0)
    return args

  
def setup(client):
  client.add_cog(Help(client))