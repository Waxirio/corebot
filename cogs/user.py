from classes.Data import Data
import discord
from discord.ext import commands
import random
import asyncio
import validators

import os
import sys
sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__),
                     os.path.pardir)))


class User(commands.Cog):

  # constructeur
  def __init__(self, client):
    self.client = client   

  # Events
  @commands.Cog.listener()
  async def on_ready(self):
    print("Cog loaded: user")

  # creation d'un nouveau player
  @commands.command()
  async def new(self, ctx):
    # recuperation des arguments
    args = self.getArgs(ctx.message.content)
    # recuperation du nom
    name = ""
    # met les elements des arguments bouts a bouts
    for n in args:
      name += n + ' '
    # envoi du message final
    if(Data.getInstance().newPlayer(ctx.message.author.id, name)):
      embed = discord.Embed(title="Perso Created !", color=1623052)
      embed.add_field(name="Resultat", value=name +
                      " a ete cree avec succes !", inline=False)
    else:
      embed = discord.Embed(title="Oops", color=12848654)
      embed.add_field(
          name="Resultat", value="Vous avez déjà un personnage \nUtilisez !rem pour le supprimer", inline=False)
    await ctx.message.channel.send(embed=embed)

  @commands.command()
  async def newimg(self, ctx):
    # init
    thumb = ""
    args = self.getArgs(ctx.message.content)
    # si il y a un argument
    if(len(args) != 0):
      thumb = str(args[0]) # le prendre en compte
    # mise a jour de l'image
    Data.getInstance().getPlayerById(ctx.author.id).setThumbnail(thumb)
    await ctx.send("Image mise a jour !")
    Data.getInstance().savePlayerById(ctx.author.id)

  # affiche les informations du perso
  @commands.command()
  async def info(self, ctx):
    # recuperation du joueur
    p = Data.getInstance().getPlayerById(ctx.message.author.id)
    # creation d'un embed
    e = discord.Embed(title="Impossible", color=1623052)
    e.add_field(
        name="Ouch", value="Impossible d'afficher votre personnage'", inline=False)
    # si il existe
    if(p != None):
      # on recupere ses informations
      e = p.getEmbed()
      if(validators.url(p._thumbnail)):
        e.set_thumbnail(url=p._thumbnail)
    # Envoi des infos
    await ctx.message.channel.send(embed=e)

  # affiche les informations du perso
  @commands.command()
  async def rem(self, ctx):
    # create embed message
    embed = discord.Embed(title="Perso deletion", color=1623052)
    # demande de suppression du joueur
    res = Data.getInstance().deletePlayerById(ctx.message.author.id)
    # envoi du message correspondant
    if(res == True):
      embed.add_field(name="Resultat:",
                      value="Supprimé avec succès", inline=False)
    else:
      embed.add_field(
          name="Resultat:", value="Impossible de supprimer le personnage", inline=False)
    await ctx.message.channel.send(embed=embed)

  # fait entrer un nouveau personnage dans le combat
  @commands.command()
  async def battle(self, ctx):
    # recuperation de l'id du chan
    channelId = ctx.channel.id
    # entrée dans le combat
    if(channelId in Data.getInstance()._currentMonster and not Data.getInstance().getMonster(channelId).isDead()):
      await asyncio.wait([self.enterBattle(channelId, ctx)]) # entree
      if(channelId in Data.getInstance()._currentMonster and Data.getInstance().getMonster(channelId).isDead()):
        # fin du combat
        await asyncio.wait([self.battleEnd(ctx)])

  async def enterBattle(self, channelId, ctx):
    # creation de l'embed
    embed = discord.Embed(title="Entrée dans le combat: ", color=15092992)
    player = Data.getInstance().getPlayerById(ctx.message.author.id)
    # Si le monstre est encore présent
    if(await Data.getInstance().newInvolvedPlayer(player, channelId) == True):
      # si c'est possible
      embed.add_field(name="Attention " + ctx.author.name,
                      value="Vous êtes entrés dans la mélée !", inline=False)
    else:
      # sinon
      embed.add_field(
          name="Attention !", value="Impossible d'entrer dans le combat...", inline=False)
    await ctx.message.channel.send(embed=embed)

  async def battleEnd(self, ctx):
    # creation de l'embed
    channelId = ctx.channel.id
    if(await Data.getInstance().checkBattleEnd(channelId) == True):
      m = Data.getInstance().getMonsterAndClean(channelId)  # set dead monster
      # calcul des gains totaux
      rewardsCoins = Data.getInstance().getCurrentMonsterLootCoins(m)
      rewardsXp = Data.getInstance().getCurrentMonsterLootXp(m)
      battleRes = ""
      for player in Data.getInstance()._involvedPlayers[channelId]:
         battleRes += await self.giveRewards(player, m, rewardsCoins, rewardsXp)
        # ajouter proportionnellement les rewards
      # reinit parameters for next fight
      embed = discord.Embed(title="Battle result", color=12848654)
      embed.add_field(
          name="Excellent !", value="Félicitation !\nLe "+ str(m._name) +" à été vaincu !\nResultat :\n"+battleRes, inline=False)
      await ctx.message.channel.send(embed=embed)
      Data.getInstance().initForNextBattle(channelId)

  async def giveRewards(self, player, monster, rewardsCoins, rewardsXp):
    # calcul des coins
    coins = round(rewardsCoins * Data.getInstance().getPlayerPercentageInBattle(player, monster))
    player.addCoins(coins)

    # calcul de l'xp
    xp = round(rewardsXp * Data.getInstance().getPlayerPercentageInBattle(player,monster))
    # nouveau niveau ?
    if(player.addXp(xp) == True):
      # send embed
      embed = discord.Embed(title="Bravo !", color=12848654)
      embed.add_field(name="Level UP !", value="Vous êtes passés au niveau suivant !", inline=False)
      # get correct user
      user = self.client.get_user(player._idPlayer)
      # if ok send embed message
      if user is not None:
        await user.send(embed=embed)

    # calcul des drops
    lblDrop = "Rien..."
    # create new embed
    drop = Data.getInstance().getCurrentMonsterLootDrop(monster)
    # s'il y en a
    if(drop != []):
      # on l'ajoute a l'inventaire
      player.addItemsToInventory(drop)
      lblDrop = ""
      # boucle dans les drops
      for d in drop:
        # ajouter au label final d'affichage
        lblDrop += d._name + "[" + str(d._amount) + "] + "
    lblDrop = lblDrop[:-2]
    # ajout de la ligne de resultat
    user = self.client.get_user(player._idPlayer)
    battleRes = "> **" + user.name + "**\t*Xp:* " + \
        str(xp) + "\t| *Seekoins:* " + str(coins) + \
        "\t| *Drops:* " + lblDrop + "\n"
    return battleRes

  # recupere les arguments d'une ligne
  def getArgs(self, texte):
    args = texte.split(' ')
    args.pop(0)
    return args

def setup(client):
  client.add_cog(User(client))
