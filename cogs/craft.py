from classes.Data import Data
import discord
from discord.ext import commands
import random
import os
import sys
import math

sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__),
                     os.path.pardir)))


class Craft(commands.Cog):

  def __init__(self, client):
    self.client = client

  # Events
  @commands.Cog.listener()
  async def on_ready(self):
    print("Cog loaded: craft")

  ########## Craft list ##########
  @commands.command()
  async def clist(self, ctx):
    # recuperation des infos
    arg = self.getArgs(ctx.message.content)
    embed = discord.Embed(title="Liste des crafts", color=12848654)
    nb = 1
    # si l'argument est un nombre
    if(len(arg) != 0 and arg[0].isnumeric()):
      nb = int(arg[0])
      if(nb > self.getNbCraftMaxPage() or nb <= 0):
        nb = 1
    # recuperation des crafts
    craft = self.getCraftFromNbPage(nb)
    # affichage du message
    embed.add_field(name="Page " + str(nb) + "/" + str(self.getNbCraftMaxPage()), value="```"+craft+"```", inline=False)
    await ctx.send(embed=embed)

  ########## Craft detail ##########
  @commands.command()
  async def cdet(self, ctx):
    # recuperation des infos
    arg = self.getArgs(ctx.message.content)
    embed = discord.Embed(title="Detail du craft", color=12848654)
    nb = 1
    # si l'argument est valide
    if(len(arg) != 0 and arg[0].isnumeric()):
      nb = int(arg[0])
      if(nb >= len(Data.getInstance()._crafts) or nb <= 0):
        nb = 0
    # on check le craft
    craft = Data.getInstance().getCraftById(nb)
    player = Data.getInstance().getPlayerById(ctx.author.id)
    # on recupere le detail du craft recupere
    craftDetail = craft.toStringWithPlayer(player)
    # affichage de l'image
    embed.add_field(name="Détail du craft " + str(nb), value=craftDetail, inline=False)
    await ctx.send(embed=embed)

  ########## Craft detail ##########
  @commands.command()
  async def craft(self, ctx):
    # recuperation des infos
    arg = self.getArgs(ctx.message.content)
    embed = discord.Embed(title="Resultat du craft", color=12848654)
    nb = None
    # Check du nombre
    if(len(arg) != 0 and arg[0].isnumeric()):
      if(int(arg[0]) < len(Data.getInstance()._crafts) and int(arg[0]) >= 0):
        nb = int(arg[0])
    # Si le nombre est valide
    result = "Impossible de réaliser le craft"
    if(nb != None):
      # On recupère le craft
      craft = Data.getInstance().getCraftById(nb)
      player = Data.getInstance().getPlayerById(ctx.author.id)
      # check si le craft est valide pour l'utilisateur
      if(craft.canBeCrafted(player)):
        # init le message
        result = "Craft effectué avec succès !\nObtenu: " + str(craft._finalItem._name)
        # ajout des elements crees
        craftedItems = []
        craftedItems.append(craft._finalItem)
        # ajout a l'inventaire
        player.addItemsToInventory(craftedItems)
        # depense des ingredients
        player.spend(craft._ingredient)
        # message
    embed.add_field(name="Résultat", value=result, inline=False)
    await ctx.send(embed=embed)
    # sauvegarde
    Data.getInstance().savePlayerById(player._idPlayer)

  def getCraftFromNbPage(self, nb):
    # recuperation des infos
    nbCraftToDisplay = 20
    # recuperation du nombre de craft de la derniere page
    if(nb == self.getNbCraftMaxPage()):
      nbCraftToDisplay = len(Data.getInstance()._crafts)%20
    # calcul du premier craft
    firstCraftToDisplay = (nb-1)*20
    res = ""
    # boucle dans les crafts
    for i in range (firstCraftToDisplay,firstCraftToDisplay+nbCraftToDisplay):
      # ajout du nom des crafts
      c = Data.getInstance().getCraftById(i)
      res += str(c._id) + ". " + c.getName() + "\n"
    return res

  # retourne le nombre de page d'un craft
  def getNbCraftMaxPage(self):
    nbCraft = len(Data.getInstance()._crafts)
    nbCraftParPage = 20
    return math.ceil(nbCraft/nbCraftParPage)

  def getArgs(self, texte):
    args = texte.split(' ')
    args.pop(0)
    return args

def setup(client):
  client.add_cog(Craft(client))
