import discord
from discord.ext import commands
import random
import validators
import math

from classes.MarketController import MarketController
from classes.Player import Player
from classes.Data import Data

import os
import sys
sys.path.append(
    os.path.abspath(
        os.path.join(os.path.dirname(__file__),
                     os.path.pardir)))


class Market(commands.Cog):

  # constructeur
  def __init__(self, client):
    self.client = client
    self._nbItemPerPage = 20

  # Events
  @commands.Cog.listener()
  async def on_ready(self):
    print("Cog loaded: market")

  # Commandes
  @commands.command()
  async def mlist(self, ctx):
    nb = self.getFirstArgsNumber(ctx.message.content)
    # si pas de page specifiee
    if(nb == None):
      # donner la premiere
      nb = 1
    liste = MarketController.getInstance()._items
    nbPage = nb
    await ctx.send(embed=self.getItemsFromNbPage(liste, nbPage))

  @commands.command()
  async def mylist(self, ctx):
    nb = self.getFirstArgsNumber(ctx.message.content)
    # si pas de page specifiee
    if(nb == None):
      # donner la premiere
      nb = 1
    liste = MarketController.getInstance().getItemsByIdPlayer(ctx.author.id)
    nbPage = nb
    await ctx.send(embed=self.getItemsFromNbPage(liste, nbPage))

  @commands.command()
  async def sell(self, ctx):
    # init variables
    idPlayer = ctx.author.id
    args = self.getArgs(ctx.message.content)
    player = Data.getInstance().getPlayerById(idPlayer)
    if(len(args) >= 3 and args[0].isnumeric() and args[1].isnumeric() and args[2].isnumeric()):
      # get item
      idItem = int(args[0])
      qty = int(args[1])
      price = int(args[2])
      player = Data.getInstance().getPlayerById(idPlayer)
      item = player.getItemById(int(args[0])).new(qty)
      # si le player a l'item
      if(player.checkHasItem(item._id, qty)):
        # add item to market
        player.spend([item])
        MarketController.getInstance().addMarketItem(idPlayer, item, price)
        Data.getInstance().savePlayerById(idPlayer)
        await ctx.send("Vente mise en ligne !")
      else:
        await ctx.send("Erreur dans la selection de l'item")
    else:
      await ctx.send("Usage: !sell idItem qty price")

  @commands.command()
  async def buy(self, ctx):
    nb = self.getFirstArgsNumber(ctx.message.content)
    player = Data.getInstance().getPlayerById(ctx.author.id)
    if(nb != None):
      marketItem = MarketController.getInstance().getItemByIdMarket(nb)[0]
      price = marketItem._price
      # si le joueur peux se le payer
      if(player.canAfford(price)):
        # il paie
        player.spendMoney(price)
        Data.getInstance().getPlayerById(marketItem._idPlayer).addMoney(price)
        # l'objet est donné
        player.addItemsToInventory([marketItem._item])
        # l'offre disparait
        MarketController.getInstance().removeMarketItemById(nb)
        Data.getInstance().savePlayerById(player._idPlayer)
        Data.getInstance().savePlayerById(marketItem._idPlayer)
        await ctx.send("L'achat à bien été effectué")
      else:
        await ctx.send("Argent insuffisant")
    else:
      await ctx.send("Usage: !buy id")

  # retourne les arguments de la commande
  def getArgs(self, texte):
    args = texte.split(' ')
    args.pop(0)
    return args

  # retourne le premier argument
  def getFirstArgsNumber(self, texte):
    res = None
    args = texte.split(' ')
    # si c'est un nombre
    if(len(args) >= 2 and args[1].isnumeric()):
      # alors c'est ce que l'on souhaite renvoyer
      res = int(args[1])
    return res

  # retourne le nombre max de page affichable
  def getNbMaxPage(self, items):
    return math.ceil(len(items)/self._nbItemPerPage)

  def getItemsFromNbPage(self, items, nb):
    # recuperation des infos
    nbItemsToDisplay = self._nbItemPerPage
    # recuperation du nombre d'items de la derniere page
    nbMaxPage = self.getNbMaxPage(items)
    if(nb > nbMaxPage and nb <= 0):
      display = discord.Embed(title="Liste du marché", color=12848654)
      # add embed properties
      display.add_field(name="Oops", value="Il semblerait que la page n'existe pas", inline=False)
    else:
      if(nb == nbMaxPage):
        nbItemsToDisplay = len(items)%self._nbItemPerPage
      # calcul du premier item
      firstItemToDisplay = (nb-1)*self._nbItemPerPage
      res = ""
      # boucle dans les items
      if(len(items) != 0):
        for i in range (firstItemToDisplay,firstItemToDisplay+nbItemsToDisplay):
          # ajout du nom des items
          mItem = items[i]
          res += str(mItem.toString()) + "\n"
      display = self.displayList(res, nb, nbMaxPage)
    return display

  # retourne l'embed
  def displayList(self, lblItems, nbPage, getNbMaxPage):
    embed = discord.Embed(title="Liste du marché", color=12848654)
      # add embed properties
    embed.add_field(name="Page " + str(nbPage) + "/" +
                    str(getNbMaxPage), value="``` "+lblItems+" ```", inline=False)
    return embed


def setup(client):
  client.add_cog(Market(client))
